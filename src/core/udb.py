import psycopg2 as psy
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

def connectDB(DBName, usr, pwd, hostname, port):
    try:
        conn = psy.connect(database=DBName.lower(), user=usr, password=pwd, port=port, host=hostname)
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    except:
        ### CREATE NEW DB ###
        conn = psy.connect(user=usr, password=pwd, port=port, host=hostname)
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur = conn.cursor()
        cur.execute('CREATE DATABASE ' + DBName.lower())
        conn.commit()
        cur.close()
        conn.close()
        ### CONNECT TO THE NEWLY CREATED DB AND CREATE POSTGIS EXT. ###
        conn = psy.connect(database=DBName.lower(), user=usr, password=pwd, port=port, host=hostname)
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur = conn.cursor()
        cur.execute('CREATE EXTENSION postgis')
        conn.commit()
        cur.close()

    return conn