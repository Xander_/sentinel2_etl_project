import requests, socket

def download(url, filename, CHUNK_SIZE = 10*1024*1024, use_stream = True, use_login = True, login = ("usr", "pwd"), use_get = False):
    
    try:
        if use_get:
            if use_login:
                r = requests.get(url, auth = login, stream = use_stream)
            else:
                r = requests.get(url, stream = use_stream)
        else:
            if use_login:
                r = requests.post(url, auth = login, stream = use_stream)
            else:
                r = requests.post(url, stream = use_stream)
        with open(filename, "wb") as f: 
            for chunk in r.iter_content(CHUNK_SIZE):
                if chunk: # filter out keep-alive new chunks
                    f.write(chunk)
    except requests.ConnectionError or requests.ConnectTimeout:
        raise
    except SystemError or OSError or Exception or KeyboardInterrupt:
        raise SystemExit
    
    return filename 

def testConnection(logger, host, port):
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(10)
        sock.connect((host, int(port)))
        sock.send("PING")
        logger.info("Received %s from %s:%d", str(sock.recv(4096)).upper(), host, int(port))
        sock.close()
    except:
        return False
    return True

def getLowErrorList():
     return {"HDFS_GET_ERROR":"HDFS_GET_ERROR", "HDFS_PUT_ERROR":"HDFS_PUT_ERROR", 
             "SEN2COR_ERROR":"SEN2COR_ERROR",
             "VI_ERROR":"VI_ERROR", "":"", "HDFS_TEST_ERROR":"HDFS_TEST_ERROR",
             "GDAL_TRANSFORM_ERROR":"GDAL_TRANSFORM_ERROR",
             "GDAL_RETILE_ERROR":"GDAL_RETILE_ERROR"}

def getHighErrorList():
     return {"NO_HDFS":"NO_HDFS", "NO_SEN2COR":"NO_SEN2COR",
             "UNKNOWN_ERROR":"UNKNOWN_ERROR",
             "NO_GDAL_TRANSFORM":"NO_GDAL_TRANSFORM",
             "NO_GDAL_RETILE":"NO_GDAL_RETILE"}