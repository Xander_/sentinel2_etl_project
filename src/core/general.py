"""
Created on 5 Oct 2016

@author: Xander_C
"""

import logging, datetime, string, udata, sys
from os import path as path
from os import sep as sep, makedirs as mkdir, getpid
from strict_rfc3339 import validate_rfc3339 as validate
from dateutil import parser as parser

def displayHelper():
    log = setLogger("Helper")
    log.info("Flags accepted: ")
    log.info("   download-only    Only download files from the url specified into the configuration file.")
    log.info("   recover    Recover unresolved downloads that are specified into the \"bad_downloads.txt\"file")
    log.info("   help    Show this dialog")
    log.info("   debug    Enable traceback")
    log.info("   execute-locally    Execute the whole process on the local machine.")

def setLogger(loggerName, logLevel = 10):
    s = "{logger}[PID:{pid}]".format(logger = loggerName, pid = getpid())
    logPath = udata.getPath(sys.argv[0], "ETL.log") #"_" + str(datetime.datetime.now()).replace(" ", "T")+"Z" +
    logging.basicConfig(filename = logPath, level = logLevel,
                        format = '[%(asctime)s][%(levelname)s] %(name)s: %(message)s')
    log = logging.getLogger(s)
    return log

def createProject(*paths):
    for p in paths:
        if not path.isdir(p):
            logging.info("{path} not found, creating...".format(path = p))
            mkdir(p)

#######################################################################################################

def getRFC3339FormatString():
    return "{year}-{month}-{day}T{hours}:{minutes}:{seconds}.{milliseconds}Z"

def incrementRFC3339Date(rfcDate):
    date = parser.parse(rfcDate) + datetime.timedelta(microseconds=1000)

    Y = "0" + str(date.year) if date.year < 10 else date.year
    Mo = "0" + str(date.month) if date.month < 10 else date.month
    D = "0" + str(date.day) if date.day < 10 else date.day
    H = "0" + str(date.hour) if date.hour < 10 else date.hour
    Min = "0" + str(date.minute) if date.minute < 10 else date.minute
    S = "0" + str(date.second) if date.second < 10 else date.second

    return getRFC3339FormatString().format(year = Y, month = Mo, day = D,
                                  hours = H, minutes = Min, seconds = S,
                                  milliseconds = date.microsecond)

def verifyDates(fromDate, toDate):
    #last = '2000-01-01T00:00:00.001Z'  # RFC3339
    #now = datetime.datetime.utcnow().isoformat('T') + "Z"

    if validate(fromDate):
        last = fromDate
    else: raise SyntaxError

    if validate(toDate) or toDate == "NOW":
        now = toDate
    else: raise SyntaxError

    return last, now

#######################################################################################################

def composeQuery(logger, config, fromDate, toDate, recovering = False):
    if not recovering:
        query = string.join(config["query"],"")
        if config["use ingestion date"]:
            query += ' AND ingestiondate: [{ingestion} TO {now}]'.format(ingestion = fromDate, now = toDate)
    else:
        with open(config['root'] + sep + config["recovery file"], "r") as f:
            entries = string.split(f.read(), " ")
        query = "uuid:\"{entries}\"".format(entries = string.join(entries, "\"OR uuid:\""))

    logger.info("Requested: %s", query)

    return config["url header"] + query + config["url footer"]