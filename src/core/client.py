"""
Created on 13 Oct 2016

@author: Xander_C
"""

import socket
import general, network
from udata import getJson, getPath
from os import sep
from load import Loader

class Client(object):
    
    def __init__(self, config, host, port):
        self.host = socket.gethostbyname(host) 
        self.port = int(port)
        self.__config = config
        self.BUFFER_SIZE = 4096
        self.lowErrors = network.getLowErrorList()
        self.highErrors = network.getHighErrorList()

    def __call__(self, fromFolder, filename, toFolder):
        return self.runServices(fromFolder, filename, toFolder)
    
    def runServices(self, fromFolder, filename, toFolder):
        log = general.setLogger("Client")
        tries, skip = 0, True
        data = None
        try:
            if not Loader.loadOnHDFS(fromFolder, filename, toFolder):
                log.info("File already stored on HDFS... Skipping.")

            # Repeat until it goes right. FFS.
            while tries < 10:
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                log.info("Connecting to worker server %s on port %d ", self.host, self.port)
                sock.connect((self.host, int(self.port)))
                # Sending  Elaboration request to a subscriber
                log.info("Sending: %s. Lenght: %d Byte.", filename, len(filename))
                sock.sendall(filename)
                # Wait for the response
                log.info("Waiting for a response... ")
                data = sock.recv(self.BUFFER_SIZE)
                log.info("Received: %s", str(data))

                if data in self.lowErrors.itervalues():
                    tries += 1
                else:
                    break

                log.info("Closing Connection...")
                sock.close()

        except Exception as ex:
            log.exception("%r:%s", str(ex), ex.args[0])

        if tries >= 10 or data in self.highErrors.itervalues():
            if data in self.highErrors.itervalues():
                log.error("The slave you are communicating with has %s, " +
                          "configure adequately your system.", data)
            log.info("Writing file on %s", self.__config["Malformed"])
            with open(self.__config['root'] + sep + self.__config["Malformed"], 'wb') as f:
                f.write(filename + "")

        log.info("Exiting...")
        
        return data