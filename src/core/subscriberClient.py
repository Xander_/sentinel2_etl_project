"""
Created on 21 Oct 2016

@author: Xander_C
"""

import general as gU, network as net
import socket

class SubscriberClient(object):
    
    def __init__(self, host, port):
        self.host = socket.gethostbyname(host)
        self.port = int(port)
        self.BUFFER_SIZE = 4096
        self.lowErrors = net.getLowErrorList()
        self.highErrors = net.getHighErrorList()
        self.subString = "{\"Name\":\"{hostname}\", \"IP\":\"{ip}\", \"Port\":\"{port}\", \"MB\": \"0\", \"Tasks\": \"0\"}"
        self.logger = gU.setLogger("Subscriber")
    
    def subscribe(self, host, port):
        sub = "{\"HostName\":\""+str(socket.gethostname())+\
              "\",\"IP\":\""+str(socket.gethostbyname(host))+\
              "\",\"Port\":\""+str(port)+"\",\"MB\":\"0\",\"Tasks\":\"0\"}"

        data = ""

        while data in self.lowErrors.itervalues() or data in self.highErrors.itervalues():
            # ERROR is the response sent by the server if the .json is wrong, 
            # None is for safety 
            # and "" happens when the server crashes 
            try:
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.logger.info("Connecting to subscription server %r on port %r ", self.host, self.port)
                sock.connect((self.host, int(self.port)))

                self.logger.info("Sending subscription %r", self)
                sock.sendall(sub)

                # Wait for the response
                self.logger.info("Waiting for a response... ")
                data = sock.recv(self.BUFFER_SIZE)
                self.logger.info("Received: %r", data)

                self.logger.info("Closing Connection...")
                sock.close()
            except Exception as ex:
                self.logger.exception("Failed to communicate with the server. Error: %r", str(ex))
                raise
            finally:
                self.logger.info("Exiting...")
            
        return data
    