"""
Created on 12 Oct 2016

@author: Xander_C
"""

import collections
import datetime
import general
import multiprocessing, threading

##################################################################################################################################
"""
@descr:
    A Manager for multiple watchers on a queue.
"""
class WatchersManager(object):
    
    def __init__(self, workers = 1):
        self.__workers = []
        self.__backup = []
        self.NUM_WORKERS = workers
        self.tasksQueue = multiprocessing.JoinableQueue()
        self.resQueue = multiprocessing.Queue()

    def __checkVitality(self):
        for w in self.__workers:
            if w.is_alive():
                self.__backup.append(w)

        self.__workers = self.__backup
        self.__backup = []

        return self

    def __closeQueue(self, queue):
        queue.close()
        queue.join_thread()
        return self

    def __poison(self):
        # The Poison Pill
        for _ in self.__workers:
            self.tasksQueue.put(None)
    
    def submit(self, task):
        if len(self.__workers) < self.NUM_WORKERS:
            self.__workers.append(Watcher(self.tasksQueue, self.resQueue).start())

        self.tasksQueue.put(task)

        return self.__checkVitality()

    def join(self):
        self.__poison()
        for w in self.__workers:
            w.join()

        self.__closeQueue(self.tasksQueue)
        resv = [self.resQueue.get() for i in range(0, self.resQueue.qsize())]
        self.__closeQueue(self.resQueue)

        return resv

    def terminate(self):
        self.__poison()
        for w in self.__workers:
            w.terminate()
        self.__closeQueue(self.tasksQueue).__closeQueue(self.resQueue)

"""
@desc:
    This class is a process that can run Callable Objects.
    Callable objects are objects that implements __call__(self)
    and return anything to populate the resultsQueue
"""
class Watcher(multiprocessing.Process):
    
    def __init__(self, tasksQueue, resQueue):
        multiprocessing.Process.__init__(self, target = self.run)
        self.__stayAlive = True
        self.__tasksQueue = tasksQueue
        self.__resQueue = resQueue
        self.daemon = True
    
    def run(self):
        while self.__stayAlive:
            if self.__tasksQueue.qsize() > 0:
                task = self.__tasksQueue.get()
                if task is None:
                    self.__stayAlive = False
                else:
                    self.__resQueue.put((self.pid, task()))
                self.__tasksQueue.task_done()

    def start(self):
        multiprocessing.Process.start(self)
        return self

"""
@descr:
    An example of callable obejct
    that can run any given function
    with the given parameters
"""
class Task(object):
    
    def __init__(self, callableObj, *args):
        self.__callableObj = callableObj
        self.__args = args

    def __call__(self):
        return self.__callableObj(*self.__args)
            
            
##################################################################################################################################

class ProcessesManager(object):
    
    def __init__(self, target, workers = None, maxTasks4Process = None):
        self.logger = general.setLogger("Watcher")
        self.target = target
        self.PROCESSES = workers
        self.routine =  multiprocessing.Pool(processes = self.PROCESSES, maxtasksperchild = maxTasks4Process)
        self.pool = []
            
    # run the given target asynchronously and returns the watcher of the thread
    def submit2Watcher(self, arguments = ()):
        self.pool.append(self.routine.apply_async(self.target, args = arguments))
        return self
        
    def getRoutine(self):
        return self.routine
    
    def closeSubmission(self):
        self.routine.close()
        return self
    
    def joinProcesses(self):
        self.routine.join()
        return self
    
    def terminateProcesses(self):
        self.routine.terminate()
        return self
    
    def wait(self):
        for task in self.pool:
            task.wait()
            value = task.get()
            self.logger.debug(" Task: " + str(task) + " has ended execution returning " + str(value) + "\n")
        
    def test(self):
        count = 0
        for task in self.pool:
            if task.ready():
                count += 1
        
        if count == len(self.pool):
            return True
        else:
            return False
    
    def get(self):
        for task in self.pool:
            task.get()

##################################################################################################################################

class ThreadsManager(object):
    
    def __init__(self, group = None, target = None, name = None, verbose = None, maxThreads = 4):
        
        self.lock = multiprocessing.Lock()
        self.queue = collections.deque()
        self.pool = collections.deque()
        self.MAXNUMTHREADS = maxThreads
        self.target = target
        self.group = group
        self.name = name
        self.verbose = verbose
        self.dieAll = False
        
    def submit2Watcher(self, args = (), kwargs = None):
        
        if len(self.queue) <= self.MAXNUMTHREADS:
            self.pool.append(threading.Thread(group = self.group, target = self.execute, name = self.name, verbose = self.verbose))
        with self.lock:
            self.queue.append(args)
    
    def execute(self):
        while self.dieAll is not True:
            args = None
            with self.lock:
                if len(self.queue) > 0:
                    args = self.queue.popleft()
            if args is not None:
                self.target(*args)

##################################################################################################################################
class Test(object):

    def __init__(self):
        pass

    def __call__(self, n):
        return self.testFunction(n)

    def testFunction(self, n):
        if n % 2 == 0:
            return n, False, datetime.datetime.utcnow().isoformat('T') + "Z"

        sqrt_n = int(n**0.5)
        for i in range(3, sqrt_n + 1, 2):
            if n % i == 0:
                return n, False, datetime.datetime.utcnow().isoformat('T') + "Z"
        return n, True, datetime.datetime.utcnow().isoformat('T') + "Z"

##################################################################################################################################

if __name__ == "__main__":

    import os, platform

    logger = general.setLogger(os.getpid())

    argsv = [
        112272535095293,
        112582705942171,
        112272535095297,
        115280095190773,
        115797848077099,
        1099726899285419,
        103384737489293,
        19827373838289,
        1827364621,
        1928373727183,
        112272535095293,
        112582705942171,
        112272535095297,
        115280095190773,
        115797848077099,
        1099726899285419,
        103384737489293,
        19827373838289,
        1827364621,
        1928373727183]

    prc_num = 4

    if platform.system() == "Windows":
        multiprocessing.freeze_support()
        #time.sleep(60)

    logger.info("I'm alive")

    pool = WatchersManager(workers = prc_num)
    #pool = FutureProcessesWatcher(target = testFunction, workers = prc_num)

    for arg in argsv:
        logger.info("submitting prime: %d", arg)
        pool.submit(Task(Test(), arg))

    res =  pool.join()

    for el in res:
        print el