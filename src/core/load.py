import general as gen, udb, udata
from os import listdir, sep, remove
from subprocess import call
import psycopg2 as psy
import shutil, subprocess, network as net
from os.path import isdir

class Loader(object):
    lowErrors = net.getLowErrorList()
    highErrors = net.getHighErrorList()
    log = gen.setLogger("Loader")

    def __init__(self, root, config):
        ###################### INITIALIZATION => This will go into a cfg.json file ##############################################
        # 0. Should I use a different configuration.json file? Maybe...
        # The metadata files (in load_raster_file) are equals for all the tiled data!

        self.__config = config
        self.getUpDir = self.__config["shared"] + sep + self.__config["getUpFolder"]
        self.workspace = self.__config["shared"] + sep + self.__config["TilesFolder"]
        self.L2ADir = self.__config["shared"] + sep + self.__config["L2AFolder"]
        self.delDir = self.__config["shared"] + sep + self.__config["deleteFolder"]
        gen.createProject(self.L2ADir, self.getUpDir, self.delDir, self.workspace)

        self.geoRoot = "C:\\Program Files (x86)\\GeoServer 2.9.0"
        self.mosaic = self.geoRoot + sep + "webapps\\geoserver\\WEB-INF\\lib\\gt-imagemosaic-jdbc-15.0.jar"
        self.geoData = self.geoRoot + sep + "data_dir"

        self.__jdbcMetaConfig = udata.getPath(root, "meta.conf.postgis.xml")
        self.__jdbcMetaConnect = udata.getPath(root, "meta.connect.postgis.xml.inc")
        self.__jdbcMetaMapping = udata.getPath(root, "meta.mapping.postgis.xml.inc")

        self.__scriptsDir = self.geoData + sep + "coverages\\{filename}"
        self.__jdbcConfig = self.__scriptsDir + sep + "conf.postgis.xml"
        self.__jdbcConnect = self.__scriptsDir + sep + "connect.postgis.xml.inc"
        self.__jdbcMapping = self.__scriptsDir + sep + "mapping.postgis.xml.inc"
        self.__createQuery = self.__scriptsDir + sep + "createmeta.sql"
        self.__insertQuery = self.__scriptsDir + sep + "add_{filename}.sql"

    def __call__(self, *args):
        self.load(*args)

    @staticmethod
    def loadOnHDFS(fromLocalFolder, filename, toHDFSFolder, removeFromLocal=True):
        exc = False
        try:
            path = fromLocalFolder + sep + filename
            if not Loader.test(toHDFSFolder + sep + filename, mode="d"):
                # Load file on HDFS
                Loader.log.info("Exporting %s on HDFS:/%s... ", fromLocalFolder + sep + filename, toHDFSFolder)
                subprocess.call(["hdfs", "dfs", "-put", path, toHDFSFolder])
                exc = True
        except OSError:
            raise Exception(Loader.highErrors["NO_HDFS"])
        except WindowsError or SyntaxError:
            raise Exception(Loader.lowErrors["HDFS_GET_ERROR"])
        except:
            raise

        # Delete unzipped file
        if removeFromLocal:
            shutil.rmtree(path, ignore_errors=True)

        return exc

    @staticmethod
    def getFromHDFS(fromHDFSFolder, filename, toLocalFolder):
        exc = False
        try:
            path = fromHDFSFolder + sep + filename
            if not isdir(toLocalFolder + sep + filename):
                if Loader.test(path, mode="d"):
                    Loader.log.info("Getting HDFS:/%s to %s... ", fromHDFSFolder + sep + filename, toLocalFolder)
                    subprocess.call(["hdfs", "dfs", "-get", path, toLocalFolder])
                exc = True
        except OSError:
            raise Exception(Loader.highErrors["NO_HDFS"])
        except WindowsError or SyntaxError:
            raise Exception(Loader.lowErrors["HDFS_PUT_ERROR"])
        except:
            raise
        return exc

    @staticmethod
    def move(fromFolder, toFolder):
        Loader.log.info("Moving %s to %s... ", fromFolder, toFolder)
        try:
            gen.createProject(toFolder)
            shutil.move(fromFolder, toFolder)
        except KeyboardInterrupt:
            raise KeyboardInterrupt
        except OSError or Exception:
            raise Exception(Loader.highErrors["UNKNOWN_ERROR"])

    @staticmethod
    def test(path, mode="e"):
        Loader.log.info("Testing existence of %s... ", path)
        try:
            exc = True if subprocess.call(["hdfs", "dfs", "-test", "-" + mode, path]) == 0 else False
        except OSError:
            raise Exception(Loader.highErrors["NO_HDFS"])
        except WindowsError or SyntaxError:
            raise Exception(Loader.lowErrors["HDFS_TEST_ERROR"])

        return exc

    def load(self, DBName, usr, pwd, hostname, port):
        self.log.info("Connecting to PostGIS Geo-DB...")

        self.log.info("Found %d tiled images to load.", len(listdir(self.workspace)))
        for item in listdir(self.workspace):
            self.log.info(item)

        conn = udb.connectDB(DBName, usr, pwd, hostname, port)

        for item in listdir(self.workspace):
            #S2A_OPER_PRD_MSIL1C_PDMC_20151204T202124_R065_V20151204T103126_20151204T103126_10m
            #realname = item.replace(" ", "_").replace("A_USER_PRD_MSIL2A_PDMC", "")\
			#	.replace("_R", "_").replace("_V", "_").replace("T", "").replace("m","").lower()
            #S2_20151204202124_065_20151204103126_20151204103126_10
            #supp = realname.split("_")
            #realname = supp[0] + "_" + supp[1] + "_" + supp[3] + "_" + supp[5]

            # Create a folder to store the scripts relative to the given Tiled Image
            dataPath = self.__scriptsDir.format(filename=item)
            JDBCcfg = self.__jdbcConfig.format(filename=item)
            tilesDir = self.workspace + sep + item

            gen.createProject(dataPath)

            # Modify the CoverageName in osm.postgrs.xml and create a local one
            # Through .xml injection of code = ILLEGAL at least in 180 countries
            with open(self.__jdbcMetaConfig, "rb") as meta:
                with open(self.__jdbcConfig.format(filename=item), "wb") as xmlf:
                    xmlf.write(meta.read().format(filename=item))

            # Create a local connect config
            with open(self.__jdbcMetaConnect, "rb") as meta:
                with open(self.__jdbcConnect.format(filename=item), "wb") as xmlf:
                    xmlf.write(meta.read().format(database=DBName.lower()))

            # Create a local mapping config
            with open(self.__jdbcMetaMapping, "rb") as meta:
                with open(self.__jdbcMapping.format(filename=item), "wb") as xmlf:
                    xmlf.write(meta.read().format(tablename=item))

            # scriptsDir -> the path where to stores the scripts
            # osm.postgis.xml => usata dallo script per referenziare:
            #   # 1. mapping.postgis.xml.inc => usata dallo script per generare lo script per Mosaic
            #   # 2. connect.postgis.xml.inc => usata dallo script per connettersi al DB
            # Che debba cambiare ogni volta il CoverageName con quello della Directory contenente le Tiles?
            self.log.info("Queries creation...")
            exc = call([token.format(GTImageMosaicJDBC=self.mosaic, cfgfile=JDBCcfg,
                                     scriptsDir=dataPath, filename=item)
                        for token in self.__config["JDBCCreate"]])

            if exc != 0:
                self.log.error("Failed to create the scripts: %r", exc)
                exit(1)

            cur = conn.cursor()
            # Execute the scripts: Creation of the table Mosaic
            #   => Maybe this should be done only once... If we want a single Table with all the images
            try:
                with open(self.__createQuery.format(filename=item), "rb") as f:
                    cur.execute(f.read().replace("MASTER_PK", item + "_MASTER_PK"))
                conn.commit()
            except psy.Error as ex:
                self.log.error("Error: %s", ex.message)

            # Execute the scripts: Insert data into the created table
            with open(self.__insertQuery.format(filename=item), "rb") as f:
                for line in f.readlines():
                    if len(line) > 2:  # It doesn't contains only "\r\n"
                        self.log.info("Executing query: %s", line)
                        try:
                            cur.execute(line)  # Should I format the string to remove the starting space?
                            conn.commit()
                        except psy.Error as ex:
                            self.log.error("Failed to exec: %s. Error: %s", line, ex.message)

            conn.commit()
            cur.close()

            call([token.format(GTImageMosaicJDBC=self.mosaic, cfgfile=JDBCcfg,
                               tilesDir=tilesDir, filename=item)
                  for token in self.__config["JDBCLoad"]])
            # Move the tiles where the manager on the cluster can pull the and load'em on HDFS
            Loader.move(tilesDir, self.getUpDir)

        conn.close()

        return self

    def pullFromSharedAndPushToHDFS(self):
        self.log.info("Found %d tiled images to load on hdfs...", len(listdir(self.getUpDir)))
        for l2a in listdir(self.delDir):
            remove(self.delDir + sep + l2a)

        for item in listdir(self.getUpDir):
            try:
                Loader.loadOnHDFS(self.getUpDir, item, self.__config["HDFSTiles"])
            except Exception or OSError as ex:
                self.log.exception("Error: %s", ex.args[0])

        return self