import udata
from os import listdir, sep, pathsep as psep, environ
import sys
import transform as T
import load

### Script to run on the PostGis Server that will create the tables for the database and load the tiled files
if __name__ == "__main__":
    config = udata.getJson(udata.getPath(sys.argv[0], "configuration.json"))
    loader = load.Loader(sys.argv[0], config)

    if not "flushToHDFS" in sys.argv:
        # First transform all the L2A.SAFE files into tiled GeoTiff
        # if not "noOSGeo" in sys.argv:
        #    environ["GEOSERVER_HOME"] = "C:\\Program Files (x86)\\GeoServer 2.9.0"
        #    environ["JAVA_HOME"] = "C:\\Program Files\\Java\\jdk1.8.0_101"
        #    environ["OSGeo4W_ROOT"] = "C:\\OSGeo4~1"
        #    environ["JDK_HOME"] = environ["JAVA_HOME"]
        #    environ["JRE_HOME"] = environ["JAVA_HOME"] + sep + "jre"
        #    environ["PATH"] += psep + environ["JAVA_HOME"] + sep + "bin" + \
        #                       psep + "C:\\ProgramData\\Oracle\\Java\\javapath" + \
        #                       "C:\\OSGEO4~1\\apps\\Python27\\Scripts;C:\\OSGEO4~1\\bin"
        #    environ["CLASSPATH"] = environ["JAVA_HOME"] + sep + "lib" +\
        #						   psep + environ["JAVA_HOME"] + sep + "jre" + sep + "lib" + \
        #                           psep + environ["GEOSERVER_HOME"] + sep + "lib" + \
        #                           psep + environ["GEOSERVER_HOME"] + sep + "webapps" + sep + "geoserver" + sep + "WEB-INF" + sep + "lib"

        #    print environ

        if not "loadOnly" in sys.argv:
            load.Loader.log.info("Starting transformation of L2A.SAFE files into Tiled Geotiff packages...")
            for L2AFolderName in listdir(loader.L2ADir):
                T.Transformer(config).L2AReTiles(L2AFolderName, onCluster=False, useHDFS=False)
                break

        if not "tilesOnly" in sys.argv:
            load.Loader.log.info("Starting load of tiles geotiff imagery on PostGIS...")
            loader.load(DBName="MoreFarming", usr="postgres",
                        pwd="p0st615*", hostname="localhost", port="5432")

    elif "flushToHDFS" in sys.argv:
        loader.log.info("Pulling all the tiled files from shared directory and pushing them to HDFS...")
        loader.pullFromSharedAndPushToHDFS()
    else:
        loader.log.error("Wrong parameters!")

    exit(0)