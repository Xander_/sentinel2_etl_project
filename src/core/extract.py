"""
Created on 3 Oct 2016

@author: Xander_C
"""

import collections, multiprocessing
from os import sep, remove
import platform, requests, zipfile, xml.etree.ElementTree as elT
from client import Client
import general as gen, network, udata, load
from watchers import WatchersManager, Task
from transform import Transformer

class Extractor(object):

    def __init__(self, configFilePath, trackerFilePath,
                 recovering = False, dlOnly = False, doLocally = False):

        self.__CHUNK_SIZE, self.__MAX_TRIES = 1024 * 1024, 2
        self.__recovering, self.__dlOnly, self.__doLocally = recovering, dlOnly, doLocally

        self.__log = gen.setLogger("Extractor")

        self.__configFilePath = configFilePath
        self.__trackerFilePath = trackerFilePath

        try:
            self.__config = udata.getJson(self.__configFilePath)
        except:
            self.__log.error("%s is absent! Exiting...", self.__configFilePath)
            raise SystemExit
        finally:
            self.__log.debug("Configuration file %s has been loaded!", self.__configFilePath)

        self.__root = self.__config['root'] + sep
        self.__L1CDir = self.__root + self.__config['L1CFolder']
        self.__L2ADir = self.__root + self.__config['L2AFolder']
        self.__L3ADir = self.__root + self.__config['L3AFolder']
        self.__tilesDir = self.__root + self.__config['TilesFolder']
        self.__dlPath = self.__root + self.__config['downloads'] + sep + "{identifier}.zip"
        self.__username, self.__password = self.__config['username'], self.__config['password']

        # Circular queue of the distribution of the execution to the hosts
        try:
            self.__tracker = collections.deque(udata.getJson(self.__trackerFilePath)["hosts"])
        except:
            self.__log.exception("No tracker list found!")
            raise SystemExit
        finally:
            self.__log.debug("Tracker file %s has been loaded!", self.__trackerFilePath)

        if platform.system() == "Windows":
            multiprocessing.freeze_support()

        self.__clientsManager = WatchersManager(workers=min(2 * multiprocessing.cpu_count(), len(self.__tracker)))
        self.transformer = Transformer(self.__config)

    def getItems(self):

        gen.createProject(self.__root, self.__root + self.__config['downloads'], self.__L1CDir,
                          self.__L2ADir, self.__L3ADir, self.__tilesDir)

        ###################################### DOWNLOAD .xml RESPONSE #################################################
        try: # Composing the query through conf.json file
            fromDate, toDate = gen.verifyDates(self.__config["From Date"], self.__config["To Date"])
            self.__log.info("Selected from %s to %s", fromDate, toDate)
            items = udata.getFullXMLResponse(gen.composeQuery(self.__log, self.__config, fromDate,
                                                              toDate, self.__recovering),
                                             self.__root + "copernicus_response.xml",
                                             credentials=(self.__username, self.__password),
                                             tags=udata.getTags())
            # This way I can get Ascending order of the items for SURE
            items.sort(cmp=lambda x, y: cmp(x['ingestiondate'], y['ingestiondate']), key=None, reverse=False)
            self.__log.info("Found: %d new entries.", len(items))
        except KeyboardInterrupt or SystemExit:
            raise SystemExit("Error while downloading the .xml file!")
        except elT.ParseError:
            raise SystemExit("Error while Parsing .xml file! The remote server is probably offline")
        except SyntaxError:
            raise SystemExit("Selected dates are malformed, check that they are inf ISO-8601/RFC3339 format!")
        except:
            raise SystemExit("Unknown error occurred while parsing or communicating with the remote server!")

        return items

    def extractItems(self, items):

        offlineWorkers, doLocally, skip, remoteIsOffline = 0, False, False, False

        ########################################## DOWNLOAD DATA #######################################################
        for data in items:
            # saving Ingestion date so, if the script fails, it will restart from this data file
            if self.__config["use ingestion date"] and not self.__recovering:
                self.__config["From Date"] = data["ingestiondate"]
                self.__config = udata.updateJson(self.__configFilePath, self.__config)
                self.__log.info("Date Updated to: %s", self.__config["From Date"])

            self.__log.info("\n    Downloading: %s\n    UUID: %s\n    Ingestion Date: %s\n    Size: %s\n",
                            data["identifier"], data["uuid"], data["ingestiondate"], data["size"])

            zippedFile = self.__dlPath.format(identifier=data["identifier"])

            try:
                tries = 0
                # Max 3 tentatives
                while tries < self.__MAX_TRIES:
                    # Launch download request
                    network.download(self.__config["download url"].format(uuid=data["uuid"]), zippedFile,
                                     login=(self.__username, self.__password), use_get=True)
                    # CRC integrity's check, can be either done with subprocesses or 7z
                    self.__log.info("Verifying CRC code... ")
                    if udata.validateZip(zippedFile):
                        self.__log.info("No errors found!")
                        tries = self.__MAX_TRIES
                    elif tries > self.__MAX_TRIES:
                        self.__log.warn("Adding uuid to recovery file!")
                        with open(self.__root + self.__config["Recovery"], 'wb') as f:
                            f.write(data["uuid"] + "")
                    else:
                        self.__log.error("Errors detected while verifying the .zip file, download re-started.")
                        tries += 1

            except requests.ConnectionError or requests.ConnectTimeout:
                # If there is a connection error, then is useless to continue
                raise SystemExit("Remote server has closed the connection")
            except SystemExit or KeyboardInterrupt or OSError:
                raise SystemExit("Interrupt received, Terminating spawned tasks... ")
            except:
                raise SystemExit("An Unknown error has occurred, closing the application... ")

            # Proceding with the extraction of the file
            self.__log.info("Unzipping the data image file")
            try:
                ex_code = udata.unzip(zippedFile, self.__L1CDir)

                if ex_code is not None or ex_code:
                    self.__log.info("Removing original .zip file")
                    remove(zippedFile)
                else:
                    self.__log.error("There has been an error unzipping the file!")
            except KeyboardInterrupt:
                raise SystemExit("Interrupt received, Terminating spawned tasks... ")
            except zipfile.BadZipfile:
                self.__log.warn("Seems that the file is corrupted in some way, adding its UUID to recovery file!")
                with open(self.__root + self.__config["Recovery"], 'wb') as f:
                    f.write(data["uuid"] + "")
                skip = True

            if not skip:
                filename = data["identifier"] + ".SAFE"
###################################################### DISTRIBUTE WITH SIMPLE A CLIENT-SERVER MODEL ###################
                if not self.__dlOnly and not self.__doLocally and len(self.__tracker) > 0:
                    # Get the next IP which will process the transformation
                    host = self.__tracker.popleft()
                    self.__log.info("Testing connection to host %s on port %d", host["HostName"], int(host["Port"]))
                    # Test if the subscribed host is offline
                    while not network.testConnection(self.__log, host["HostName"], int(host["Port"])):
                        offlineWorkers += 1
                        self.__log.info("Host %s on port %d is offline", host["HostName"], int(host["Port"]))
                        self.__tracker.append(host)
                        host = self.__tracker.popleft()
                        # if the hosts are all offline then execute the script locally
                        if offlineWorkers >= len(self.__tracker):
                            doLocally = True
                            break

                    if not doLocally:
                        self.__log.info("Spawning %s elaboration on client connected to %s!", filename, host["HostName"])
                        self.__clientsManager.submit(Task(Client(self.__config, host["HostName"], int(host["Port"])),
                                                          self.__L1CDir, filename, self.__config['HDFSL1C']))
                    self.__tracker.append(host)
                else:
                    doLocally = True

################################################ OLD FASHION WAY: SERIALIZED ##########################################
                if doLocally:
                    # I'll try every time to use distributed services above the local computation
                    doLocally, offlineWorkers = False, 0

                    if not load.Loader.loadOnHDFS(self.__L1CDir, filename, self.__config['HDFSL1C'], not self.__dlOnly):
                        self.__log.info("File already stored on HDFS... Skipping.")

                    if not self.__dlOnly:
                        # Uploading the Data Image X on HDFS
                        try:
                            L2AFileName = self.transformer.L1CToL2A(filename, pushL2AToShared=True)
                            #self.transformer.L2AReTiles(L2AFileName)
                        except OSError or WindowsError or SyntaxError or KeyboardInterrupt or Exception as exe:
                            self.__log.exception("%s: %s ", exe, exe.args[0])
                            self.__log.info("Writing file on %s", self.__config["Malformed"])
                            with open(self.__root + self.__config["Malformed"], 'wb') as f:
                                f.write(filename + "")
                    else:
                        load.Loader.move(self.__L1CDir+sep+filename, self.__config["shared"]+ sep + self.__config["L2AFolder"])

        try:
            if self.__config["use ingestion date"] and not self.__recovering:
                self.__config["From Date"] = gen.incrementRFC3339Date(self.__config["From Date"])
                self.__config["Now Date"] = "NOW"
                self.__config = udata.updateJson(self.__configFilePath, self.__config)
                self.__log.info("Ingestion Date Update to: %s", self.__config["From Date"])
        except:
            raise

        return 0

#######################################################################################################################

    def joinWorkers(self):
        # Joining the asynchronously spawned routines
        self.__log.info("Joining spawned tasks...")
        for res in self.__clientsManager.join():
            self.__log.info(str(res))

#######################################################################################################################

    def terminateWorkers(self):
        self.__clientsManager.terminate()
