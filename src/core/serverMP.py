"""
Created on 18 Oct 2016

@author: Xander_C
"""

import SocketServer
from subscriberClient import SubscriberClient
import sys, logging
import socket
import general as gen, udata
from transform import Transformer

class RequestHandler(SocketServer.StreamRequestHandler):
    
    def __init__(self, request, client_address, server):
        SocketServer.StreamRequestHandler.__init__(self, request, client_address, server)
        
    def handle(self):
        logger = gen.setLogger("Requests Handler")
        transformer = Transformer(udata.getJson(udata.getPath(sys.argv[0], "configuration.json")))

        try:
            filename = str(self.request.recv(4096))
            logger.info("Received %s", filename)
            if filename.upper() == "PING":
                feedback = "PONG"
            else:
                feedback = transformer.L1CToL2A(filename, pushL2AToShared=True)
            #feedback = transformer.L2AReTiles(feedback)
        except OSError or WindowsError or SyntaxError or KeyboardInterrupt or Exception as ex:
            feedback = ex.args[0]
            logger.exception("%r:%s", str(ex), feedback)

        self.request.send(feedback)
        logger.debug("Closing...")
        self.request.close()

        return feedback

class ReactiveServerMP(SocketServer.ForkingMixIn, SocketServer.ForkingTCPServer):
    
    def __init__(self, (host, port), RequestHandlerClass = RequestHandler, bind_and_activate = False):
        SocketServer.ForkingTCPServer.__init__(self, (socket.gethostbyname(host), int(port)), RequestHandlerClass, bind_and_activate)
        self.host = socket.gethostbyname(host)
        self.port = int(port)
        self.logger = logging.getLogger("Server")
        
    def listen(self):
        self.logger.info("Launching main server %r on port: %r", self.host, self.port)
        self.allow_reuse_address = True
        self.server_bind()
        self.server_activate()
        self.serve_forever()
        return self
    
    def subscribe(self, subscriptionHost, subscriptionPort):
        try:
            SubscriberClient(subscriptionHost, int(subscriptionPort)).subscribe(self.host, self.port)
        except Exception as ex:
            self.logger.exception("%r:%s", str(ex), ex.args[0])
        finally:    
            return self
          
if __name__ == "__main__":
    
    if len(sys.argv) == 5:
        rs = ReactiveServerMP((sys.argv[3], int(sys.argv[4])))
        rs.subscribe(sys.argv[1], int(sys.argv[2]))
        rs.listen()
        rs.shutdown()
        rs.server_close()
    else:
        logging.error("Number of arguments is wrong!")
        logging.error("python serverMP.py <subscriptionServer IP> <subscriptionServer Port> <myIP> <myPort>")
        
    logging.info("Exiting... ")
    
### END ###
    