
import sys, subprocess
import general as gen, udata
from extract import Extractor
import load

if __name__ == "__main__":
    recovering, dlOnly, doLocally = False, False, False

    if "help" in sys.argv:
        gen.displayHelper()
        exit(0)
    if "debug" in sys.argv:
        sys.tracebacklimit = None
    else:
        sys.tracebacklimit = 0
    if "recover" in sys.argv:
        recovering = True
    if "download-only" in sys.argv:
        dlOnly = True
    if "execute-locally" in sys.argv:
        doLocally = True

    #log = gen.setLogger("Main")
    configfilepath = udata.getPath(sys.argv[0], "configuration.json")
    extractor = Extractor(configfilepath,
                          udata.getPath(sys.argv[0], "tracker.json"),
                          recovering, dlOnly, doLocally)
    try:
        #log.info("Verifying the presence of files to be picked up into the shared directory...")
        load.Loader(sys.argv[0], udata.getJson(configfilepath)).pullFromSharedAndPushToHDFS()
    except:
        pass

    try:
        exitcode = extractor.extractItems(extractor.getItems())
        subprocess.call(["chmod", "-R", "777", "/mnt/morefarming/L2A/*"])
    except SystemExit as ex:
        #log.exception("%r:%s", str(ex), ex.args[0])
        exitcode = 1
    finally:
        extractor.joinWorkers()
    #print "Exiting..."
    exit(exitcode)