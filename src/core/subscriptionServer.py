"""
Created on 20 Oct 2016

@author: Xander_C
"""

import collections
import sys, logging
import socket
import threading
import general, network, udata
import json

class SubscriptionServer(object):
    
    def __init__(self, (host, port)):
        
        self.host = socket.gethostbyname(host)
        self.port = int(port)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))
        self.queue = collections.deque()
        self.BUFFER_SIZE = 4096
        self.logger = general.setLogger("Subscription")

    def listen(self, root, trackerFile):
        
        subscriber = threading.Thread(target = self.subscriptionHandler, args = (root, trackerFile))
        subscriber.daemon = True
        self.logger.debug("Spawning subscription handler...")
        subscriber.start()
        self.logger.info("Launching main server %s on port: %d", self.host, self.port)
        self.sock.listen(10)
        
        while True:
            self.logger.debug("Waiting for a connection... ")
            conn, addr = self.sock.accept()
            feedback = "SUBSCRIBED: {host},{port}"
            # The response is supposed to be a string in a .json format!
            # '{"Name":"localhost", "IP": "0.0.0.0", "Port": "0", "MB": "0", "Tasks": "0"}'
            data = str(conn.recv(self.BUFFER_SIZE))
            try:
                subscription = json.loads(data)
                self.logger.info("Received subscription for %s %s on port %d", subscription["HostName"], subscription["IP"], int(subscription["Port"]))
                self.queue.append(subscription)
            except ValueError:
                self.logger.error("Invalid Subscription message!")
                feedback = "BAD_FORMAT"
            finally:
                conn.send(feedback.format(host=subscription["HostName"],
                                          port=subscription["Port"]))
                
            if not subscriber.isAlive():
                subscriber = threading.Thread(target = self.subscriptionHandler, args = ())
                subscriber.daemon = True
                self.logger.debug("Spawning subscriber thread")
                subscriber.start()

        self.sock.close()
        return self
            
    def subscriptionHandler(self, root, trackerFile):

        logger = general.setLogger("Subscription Handler")
        
        try:

            tracker = udata.getJson(udata.getPath(root, trackerFile))
            while True:
                if len(self.queue) > 0:
                    subscribed = self.queue.popleft()
                    i = 0
                    while i < len(tracker["hosts"]):
                        if tracker["hosts"][i]["IP"] == subscribed["IP"]:
                            logging.info("Subscription for %s already present into the tracker file.", subscribed["IP"])
                            # I suppose there is only 1 subscriber per host
                            if tracker["hosts"][i]["Port"] != subscribed["Port"]:
                                # Aggiorno la porta dell'iscritto
                                logging.info("Updating subscription Port from %d to %d", 
                                             int(tracker["hosts"][i]["Port"]), int(subscribed["Port"]))
                                tracker["hosts"][i]["Port"] = subscribed["Port"]
                                udata.updateJson(udata.getPath(root, trackerFile), tracker)
                            break # Interrompo se ho trovato una corrispondenza
                        i += 1
                    
                    if i == len(tracker["hosts"]):
                        logging.info("Adding new subscription to the tracker file.")
                        tracker["hosts"].append(subscribed)
                        udata.updateJson(udata.getPath(root, trackerFile), tracker)
            
        except SystemExit:
            logger.exception("The tracker File is absent")
        finally:
            logger.debug("Closing... ")
            
        return 0
          
if __name__ == "__main__":
    
    if len(sys.argv) == 3:
        rs = SubscriptionServer((sys.argv[1], int(sys.argv[2]))).listen(sys.argv[0], "tracker.json")
    else:
        logging.error("Not enough arguments")
        
    logging.info("Exiting... ")
    
    exit(0)
### END ###
    
    