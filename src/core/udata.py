import os, string, json, platform
import subprocess
import zipfile, network
import xml.etree.ElementTree as elT

def getTags():
    return {"str":{"name":["identifier", "size"]},
            "id":{"":["uuid"]},
            "date":{"name":["ingestiondate"]}}

'''
@note: props should be in the form of a dictionary with a
        structure as showed by the getTags() function

'''
def parseXML(xmlFile, tags = {}):
    
    tree = elT.parse(xmlFile)
    root = tree.getroot()

    offset = "{http://www.w3.org/2005/Atom}"

    dataVector = []
    try:
        for xmlEntry in root.findall(offset+'entry'):
            # looking for uuid and filename
            entry = {}
            for tag in tags.iteritems():
                for xmlEl in xmlEntry.findall(offset + tag[0]):
                    # ex.: get all tags with tag "str"
                    for attr in tag[1].iteritems():
                        # of those tags get the ones with the specified attribute "name" == "xxx"
                        for value in attr[1]:
                            if attr[0] == "" or value == "" or xmlEl.get(attr[0]) == value:
                                entry.update({value : xmlEl.text})

            # Append the entry into the data vector
            dataVector.append(entry)
    except:
        raise elT.ParseError
    
    return dataVector

def getFullXMLResponse(formattableURL, XMLFileName, credentials = ("User", "Password"), tags = {}):
    row, stop, dataVector = 0, False, []

    try:
        while not stop:
            url = formattableURL.format(nrows=100, fromrow=row)
            network.download(url, XMLFileName, login=credentials)
            # PARSING .xml RESPONSE
            dataVector.extend(parseXML(XMLFileName, tags))
            stop = True if len(dataVector) < row else False
            row += 100
    except:
        raise

    return dataVector

def validateZip(path):
    
    # CRC integrity's check, can be either done with subprocesses or 7z
    ex_code = False
    try:
        z = zipfile.ZipFile(path, "r")
        if z.testzip() is None:
            ex_code = True
        z.close()
    except:
        return False
    return ex_code

def zipDir(file_path):
    try:
        if platform.system() == "Windows":
            ex_code = subprocess.call(["7z", "a", "-y", file_path])
        else:
            z = zipfile.ZipFile(file_path, "w", zipfile.ZIP_DEFLATED)
            for root, dirs, files in os.walk(file_path):
                for f in files:
                    z.write(os.path.join(root, f))
            ex_code = True
    except:
        raise

    return ex_code
    
def unzip(file_path, output_dir):
    try:
        if platform.system() == "Windows":
            ex_code = subprocess.call(["7z", "x", "-y" , "-o"+ output_dir, file_path]) # subprocesses = O.P. nerf pls # 
        else:
            z = zipfile.ZipFile(file_path, "r")
            z.extractall(output_dir)
            z.close()
            ex_code = True

    except:
        raise
    
    return ex_code

'''
@note: Return the path of a file located in the same directory of the script
'''
def getPath(main, filename, sep = os.sep):
    
    actual_path = string.split(main, os.sep)
    actual_path[len(actual_path)-1] = filename
    file_path = string.join(actual_path, sep)
    return file_path

def getJson(json_path):
    
    if not os.path.isfile(json_path):
        raise SystemExit
    try:
        f = open(json_path, "rb")
        json_str = f.read()
        jSon = json.loads(json_str)
        f.close()
    except TypeError:
        raise
    return jSon

def updateJson(json_path, editedJSon):
    
    f = open(json_path, "wb")
    f.write(json.dumps(editedJSon, sort_keys = True, indent = 4, separators=(',', ': ')))
    f.close()
    
    return editedJSon