from os import sep, remove
import string, subprocess, network
import shutil, general as gen
import load as L

class Transformer(object):

    def __init__(self, config):
        self.__config = config
        self.__logger = gen.setLogger("Transformer")
        self.__lowErrors = network.getLowErrorList()
        self.__highErrors = network.getHighErrorList()
        self.MODES = ["Transform", "Retile"]

    def __call__(self, mode, *args):
        exc = None
        if mode == "Transform":
            exc = self.L1CToL2A(*args)
        elif mode == "Retile":
            exc = self.L2AReTiles(*args)

        return exc

    def __L2A_Process(self, *params):
        self.__logger.info("Launching Sen2Cor computation... ")
        try:
            subprocess.call(*params)
        except OSError:
            raise OSError(self.__highErrors["NO_SEN2COR"])
        except Exception:
            raise Exception(self.__lowErrors["SEN2COR_ERROR"])
        return True

    def __gdal_transform(self, *params):
        self.__logger.info("Launching GDAL Transformation... ")
        try:
            subprocess.call(*params)
        except OSError:
            raise OSError(self.__highErrors["NO_GDAL_TRANSFORM"])
        except Exception:
            raise Exception(self.__lowErrors["GDAL_TRANSFORM_ERROR"])

    def __gdal_retile(self, *params):
        self.__logger.info("Launching GDAL Retiling... ")
        try:
            subprocess.call(*params)
        except OSError:
            raise OSError(self.__highErrors["NO_GDAL_RETILE"])
        except Exception:
            raise Exception(self.__lowErrors["GDAL_RETILE_ERROR"])

    def L1CToL2A(self, L1CFileName, pushL2AToShared = False, removeL2AFromLocal = False):
        L2AFolder = self.__config['root'] + sep + self.__config['L2AFolder']
        L1CFilePath = L2AFolder + sep + L1CFileName
        L2AFileName = string.replace(L1CFileName, "OPER_PRD_MSIL1C", "USER_PRD_MSIL2A")

        # DO THE THING
        # Get the file from HDFS to Local -> Useful for distributing this Task
        # Here it goes the call to launch the YARN task that will operate to trasform the files
        # Those are the task that should be distributed with YARN

        try:
            if L.Loader.getFromHDFS(self.__config['HDFSL1C'], L1CFileName, L2AFolder):

                # Transforms L1C -> L2A with Atmospheric Correction @ 10m, 20m, 60m resolution
                self.__L2A_Process([token.format(filepath=L1CFilePath) for token in self.__config['Sen2CorCmd']])
                # Load the new created file on hdfs and delete the local one
                L.Loader.loadOnHDFS(L2AFolder, L2AFileName, self.__config['HDFSL2A'], removeFromLocal=removeL2AFromLocal)
                shutil.rmtree(L2AFolder + sep + L1CFileName, ignore_errors=True)

                if pushL2AToShared and not removeL2AFromLocal:
                    L.Loader.move(L2AFolder+sep+L2AFileName, self.__config["shared"]+ sep + self.__config["L2AFolder"])

            else:
                self.__logger.error("The file is absent on HDFS!")

        except OSError or WindowsError or SyntaxError or Exception as  ex:
            raise Exception("%r:%s", str(ex), ex.args[0])

        return L2AFileName

    def L2AReTiles(self, L2AFileName, onCluster = True, useHDFS = True):

        #S2A_USER_PRD_MSIL2A_PDMC_20151204T202124_R065_V20151204T103126_20151204T103126_10m
        #s2_20151204202124_065_20151204103126_20151204103126_10
        supp = L2AFileName.replace(".SAFE", "").replace(" ", "_").replace("A_USER_PRD_MSIL2A_PDMC", "")\
				.replace("_R", "_").replace("_V", "_").replace("T", "").lower().split("_")

        GeoTiffName = supp[0] + "_" + supp[1] + "_" + supp[3]

        metaFileName = L2AFileName.replace("PRD_MSI", "MTD_SAF").replace(".SAFE", "")

        if onCluster:
            root = self.__config["root"]
        else:
            root = self.__config["shared"]
            gen.createProject(root+sep+self.__config["deleteFolder"])

        L2AFilePath = root + sep + self.__config["L2AFolder"] + sep + L2AFileName

        # Tile Folder = {GeoTiffPath}_{Resolution}
        # Tiled Image = {GeoTiffPath}_{Resolution}.tif
        GeoTiffPath = root + sep + self.__config["TilesFolder"] + sep + GeoTiffName
        gen.createProject(L2AFilePath, GeoTiffPath + "_" + self.__config["GDALRes"])

        metaFilePath = L2AFilePath + sep + metaFileName

        try:
            self.__gdal_transform([token.format(L2AMetaDataFile=metaFilePath, GeoTiffImagePath=GeoTiffPath,
                                                res=self.__config["GDALRes"])
                                   for token in self.__config["GDALTranslate"]])

            self.__gdal_retile([token.format(tiledGeoTiffImageFolder=GeoTiffPath, GeoTiffImagePath=GeoTiffPath,
                                             res=self.__config["GDALRes"])
                                for token in self.__config["GDALRetile"]])

            if useHDFS and onCluster:
                #### LOAD THE (folder of) TILED .tif on HDFS without removing it from local
                L.Loader.loadOnHDFS(self.__config["root"] + sep + self.__config["TilesFolder"],
                           GeoTiffPath, self.__config['HDFSTiles'], removeFromLocal= False)

            # PUSH geotifFileName_{res}[.zip] to /mnt/Windows/Shared/Morefarming/L2ATiles
            #if onCluster:
            #    L.Loader.move(GeoTiffPath + "_" + self.__config["GDALRes"],
            #                   self.__config["shared"]+sep+self.__config["TilesFolder"])

            self.__logger.info("Removing local L2A.tif file: %s", GeoTiffPath + "_" + self.__config["GDALRes"] + ".tif")
            remove(GeoTiffPath +"_" + self.__config["GDALRes"] + ".tif")

            if onCluster:
                self.__logger.info("Removing local L2A's files directory: %s", L2AFilePath)
                shutil.rmtree(L2AFilePath, ignore_errors=True)
            else:
                L.Loader.move(L2AFilePath, root+sep+self.__config["deleteFolder"])

        except OSError or WindowsError or KeyboardInterrupt or SyntaxError or Exception:
            raise

        return "QUIT"